package {{.PkgName}}

import (
	"net/http"
	"github.com/pkg/errors"

	"github.com/zeromicro/go-zero/core/logx"
	{{if .HasRequest}}
	"github.com/go-playground/validator/v10"
	{{end}}
	"github.com/zeromicro/go-zero/rest/httpx"
	{{.ImportPackages}}
	"jcyd.com/pay-service/common/xerr"
)

func {{.HandlerName}}(svcCtx *svc.ServiceContext) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		{{if .HasRequest}}var req types.{{.RequestType}}
		if err := httpx.Parse(r, &req); err != nil {
			logx.WithContext(r.Context()).Errorf("{{.HandlerName}} error:%v", err)
			err = errors.Wrap(xerr.NewErrCode(xerr.REQUEST_PARAM_ERROR), err.Error())
			svcCtx.HttpResp.HttpResultJson(r, w, nil, err)
			return
		}

		if err := validator.New().StructCtx(r.Context(), req); err != nil {
			logx.WithContext(r.Context()).Errorf("{{.HandlerName}} error:%v", err)
			err = errors.Wrap(xerr.NewErrCode(xerr.REQUEST_PARAM_ERROR), err.Error())
			svcCtx.HttpResp.HttpResultJson(r, w, nil, err)
			return
		}

		{{end}}l := {{.LogicName}}.New{{.LogicType}}(r.Context(), svcCtx)
		{{if .HasResp}}resp, {{end}}err := l.{{.Call}}({{if .HasRequest}}&req{{end}})
		{{if .HasResp}}svcCtx.HttpResp.HttpResultJson(r, w, resp, err){{else}}svcCtx.HttpResp.HttpResultJson(r, w, nil, err){{end}}
	}
}
