func (m *default{{.upperStartCamelObject}}Model) Insert(ctx context.Context, data *{{.upperStartCamelObject}}) (sql.Result,error) {
	if data.CreateTime == 0 {
		data.CreateTime = time.Now().Unix()
	}
	if data.UpdateTime == 0 {
		data.UpdateTime = time.Now().Unix()
	}
	{{if .withCache}}{{.keys}}
    ret, err := m.ExecCtx(ctx, func(ctx context.Context, conn sqlx.SqlConn) (result sql.Result, err error) {
		query := fmt.Sprintf("insert into %s (%s) values ({{.expression}})", m.table, {{.lowerStartCamelObject}}RowsExpectAutoSet)
		return conn.ExecCtx(ctx, query, {{.expressionValues}})
	}, {{.keyValues}}){{else}}query := fmt.Sprintf("insert into %s (%s) values ({{.expression}})", m.table, {{.lowerStartCamelObject}}RowsExpectAutoSet)
    ret,err:=m.conn.ExecCtx(ctx, query, {{.expressionValues}}){{end}}
	return ret,err
}

func (m *default{{.upperStartCamelObject}}Model) InsertWithTx(ctx context.Context, session sqlx.Session, data *{{.upperStartCamelObject}}) error {
	if data.CreateTime == 0 {
		data.CreateTime = time.Now().Unix()
	}
	if data.UpdateTime == 0 {
		data.UpdateTime = time.Now().Unix()
	}
	query := fmt.Sprintf("insert into %s (%s) values ({{.expression}})", m.table, {{.lowerStartCamelObject}}RowsExpectAutoSet)	
	stmtSession, err := session.Prepare(query)
	if err != nil {
		return err
	}
	defer stmtSession.Close()
	// 返回任何错误都会回滚事务
	if _, err := stmtSession.ExecCtx(ctx, {{.expressionValues}}); err != nil {
		return fmt.Errorf("%s insert error: %s",m.table, err.Error())
	}

	return nil
}

func (m *default{{.upperStartCamelObject}}Model) InsertTransactions(ctx context.Context, session sqlx.Session, info string) error {
	var items []{{.upperStartCamelObject}}
	err := json.Unmarshal([]byte(info), &items)	
	if err != nil {
		return err
	}

	for _, item := range items {
		err = m.InsertWithTx(ctx, session, &item)
		if err != nil {
			return err
		}
	}	

	return nil
}