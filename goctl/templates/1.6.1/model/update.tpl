func (m *default{{.upperStartCamelObject}}Model) Update(ctx context.Context, {{if .containsIndexCache}}newData{{else}}data{{end}} *{{.upperStartCamelObject}}) error {
	{{if .withCache}}{{if .containsIndexCache}}data, err:=m.FindOne(ctx, newData.{{.upperStartCamelPrimaryKey}})
	if err!=nil{
		return err
	}

	if newData.CreateTime == 0 {
		newData.CreateTime = time.Now().Unix()
	}
	newData.UpdateTime = time.Now().Unix()
{{else}}
	if data.CreateTime == 0 {
		data.CreateTime = time.Now().Unix()
	}
	data.UpdateTime = time.Now().Unix()
{{end}}	{{.keys}}
    _, {{if .containsIndexCache}}err{{else}}err:{{end}}= m.ExecCtx(ctx, func(ctx context.Context, conn sqlx.SqlConn) (result sql.Result, err error) {
		query := fmt.Sprintf("update %s set %s where {{.originalPrimaryKey}} = {{if .postgreSql}}$1{{else}}?{{end}}", m.table, {{.lowerStartCamelObject}}RowsWithPlaceHolder)
		return conn.ExecCtx(ctx, query, {{.expressionValues}})
	}, {{.keyValues}}){{else}}query := fmt.Sprintf("update %s set %s where {{.originalPrimaryKey}} = {{if .postgreSql}}$1{{else}}?{{end}}", m.table, {{.lowerStartCamelObject}}RowsWithPlaceHolder)
    _,err:=m.conn.ExecCtx(ctx, query, {{.expressionValues}}){{end}}
	return err
}

func (m *default{{.upperStartCamelObject}}Model) UpdateWithTx(ctx context.Context, session sqlx.Session, {{if .containsIndexCache}}newData{{else}}data{{end}} *{{.upperStartCamelObject}}) error {
	{{if .withCache}}{{if .containsIndexCache}}data, err:=m.FindOne(ctx, newData.{{.upperStartCamelPrimaryKey}})
	if err!=nil{
		return err
	}

	if newData.CreateTime == 0 {
		newData.CreateTime = time.Now().Unix()
	}
	newData.UpdateTime = time.Now().Unix()
{{else}}
	if data.CreateTime == 0 {
		data.CreateTime = time.Now().Unix()
	}
	data.UpdateTime = time.Now().Unix()
{{end}}	{{.keys}}
	query := fmt.Sprintf("update %s set %s where {{.originalPrimaryKey}} = {{if .postgreSql}}$1{{else}}?{{end}}", m.table, {{.lowerStartCamelObject}}RowsWithPlaceHolder)
	stmtSession, err := session.Prepare(query)
	if err != nil {
		return err
	}
	defer stmtSession.Close()
	// 返回任何错误都会回滚事务
    _, err = m.ExecCtx(ctx, func(ctx context.Context, conn sqlx.SqlConn) (result sql.Result, err error) {
		return stmtSession.ExecCtx(ctx, {{.expressionValues}})
	}, {{.keyValues}}){{else}}
	query := fmt.Sprintf("update %s set %s where {{.originalPrimaryKey}} = {{if .postgreSql}}$1{{else}}?{{end}}", m.table, {{.lowerStartCamelObject}}RowsWithPlaceHolder)
	stmtSession, err := session.Prepare(query)
	if err != nil {
		return err
	}
	defer stmtSession.Close()
    _,err=stmtSession.ExecCtx(ctx, {{.expressionValues}}){{end}}	
	return err
}

