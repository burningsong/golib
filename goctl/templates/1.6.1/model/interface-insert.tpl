Insert(ctx context.Context, data *{{.upperStartCamelObject}}) (sql.Result,error)
InsertWithTx(ctx context.Context, session sqlx.Session, data *{{.upperStartCamelObject}}) error
InsertTransactions(ctx context.Context, session sqlx.Session, info string) error 