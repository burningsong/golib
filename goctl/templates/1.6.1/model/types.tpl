type (
	{{.lowerStartCamelObject}}Model interface{
		{{.method}}
	}

	default{{.upperStartCamelObject}}Model struct {
		{{if .withCache}}sqlc.CachedConn{{end}}
		table string
		conn sqlx.SqlConn
	}

	{{.upperStartCamelObject}} struct {
		{{.fields}}
	}
)
