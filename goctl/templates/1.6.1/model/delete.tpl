func (m *default{{.upperStartCamelObject}}Model) Delete(ctx context.Context, {{.lowerStartCamelPrimaryKey}} {{.dataType}}) error {
	{{if .withCache}}{{if .containsIndexCache}}data, err:=m.FindOne(ctx, {{.lowerStartCamelPrimaryKey}})
	if err!=nil{
		return err
	}

{{end}}	{{.keys}}
    _, err {{if .containsIndexCache}}={{else}}:={{end}} m.ExecCtx(ctx, func(ctx context.Context, conn sqlx.SqlConn) (result sql.Result, err error) {
		query := fmt.Sprintf("delete from %s where {{.originalPrimaryKey}} = {{if .postgreSql}}$1{{else}}?{{end}}", m.table)
		return conn.ExecCtx(ctx, query, {{.lowerStartCamelPrimaryKey}})
	}, {{.keyValues}}){{else}}query := fmt.Sprintf("delete from %s where {{.originalPrimaryKey}} = {{if .postgreSql}}$1{{else}}?{{end}}", m.table)
		_,err:=m.conn.ExecCtx(ctx, query, {{.lowerStartCamelPrimaryKey}}){{end}}
	return err
}

func (m *default{{.upperStartCamelObject}}Model) GetCacheKeys(ctx context.Context, {{.lowerStartCamelPrimaryKey}} {{.dataType}}) ([]string, error) {
	{{if .withCache}}{{if .containsIndexCache}}data, err:=m.FindOne(ctx, {{.lowerStartCamelPrimaryKey}})
	if err!=nil{
		return nil, err
	}
	{{end}}
	{{.keys}}
	return []string{ {{.keyValues}} },nil 
	{{else}}
	return nil, nil
	{{end}}
}

func (m *default{{.upperStartCamelObject}}Model) DeleteCache(ctx context.Context, {{.lowerStartCamelPrimaryKey}} {{.dataType}}) error {
	{{if .withCache}}{{if .containsIndexCache}}data, err:=m.FindOneFromDB(ctx, {{.lowerStartCamelPrimaryKey}})
	if err!=nil{
		return err
	}
{{end}}	{{.keys}}
	err {{if .containsIndexCache}}={{else}}:={{end}} m.CachedConn.DelCacheCtx(ctx, {{.keyValues}})
	return err
	{{else}}
	return nil
	{{end}}
}

func (m *default{{.upperStartCamelObject}}Model) DeleteWithTx(ctx context.Context, session sqlx.Session, {{.lowerStartCamelPrimaryKey}} {{.dataType}}) error {
	query := fmt.Sprintf("delete from %s where {{.originalPrimaryKey}} = {{if .postgreSql}}$1{{else}}?{{end}}", m.table)
	stmtSession, err := session.Prepare(query)
	if err != nil {
		return err
	}
	defer stmtSession.Close()
	if _, err := stmtSession.ExecCtx(ctx, {{.lowerStartCamelPrimaryKey}}); err != nil {
		return fmt.Errorf("%s DeleteWithTx error: %s", m.table, err.Error())
	}

	err = m.DeleteCache(ctx, {{.lowerStartCamelPrimaryKey}})
	return err
}

func (m *default{{.upperStartCamelObject}}Model) DeleteTransactions(ctx context.Context, session sqlx.Session,info string) error {
	var ids []{{.dataType}}
	err := json.Unmarshal([]byte(info), &ids)
	if err != nil {
		return fmt.Errorf("{{.upperStartCamelObject}} DeleteTransactions Unmarshal ids error: %v", err)
	}

	for _, id := range ids {
		err = m.DeleteWithTx(ctx, session, id)
		if err != nil {
			return err
		}
	}

	return nil
}
