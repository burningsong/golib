Delete(ctx context.Context, {{.lowerStartCamelPrimaryKey}} {{.dataType}}) error
GetCacheKeys(ctx context.Context, {{.lowerStartCamelPrimaryKey}} {{.dataType}}) ([]string, error)
DeleteCache(ctx context.Context, {{.lowerStartCamelPrimaryKey}} {{.dataType}}) error
DeleteWithTx(ctx context.Context, session sqlx.Session, {{.lowerStartCamelPrimaryKey}} {{.dataType}}) error
DeleteTransactions(ctx context.Context, session sqlx.Session, info string) error