package gox

import (
	"fmt"
	"math/rand"
	"sync"
	"sync/atomic"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestNotifyUpdateWorker(t *testing.T) {
	var n atomic.Int32
	type args struct {
		name               string
		mode               Mode
		batchSize          int
		waitResultDuration time.Duration
		taskFunc           func()
		alertFunc          func(title, detail string)
	}
	tests := []struct {
		name string
		args args
		want int32
	}{
		{
			name: "SkipIfStillRunning",
			args: args{
				name:               "",
				mode:               SkipIfStillRunning,
				batchSize:          5,
				waitResultDuration: time.Second * 10,
				taskFunc: func() {
					time.Sleep(time.Second * 4)
					n.Add(1)
					fmt.Println(n.Load())
				},
				alertFunc: func(title string, detail string) {
				},
			},
			want: 1,
		},
		{
			name: "DelayIfStillRunning",
			args: args{
				name:               "",
				mode:               DelayIfStillRunning,
				batchSize:          5,
				waitResultDuration: time.Second * 10,
				taskFunc: func() {
					time.Sleep(time.Second * 4)
					n.Add(1)
					fmt.Println(n.Load())
				},
				alertFunc: func(title string, detail string) {
				},
			},
			want: 2,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			n.Store(0)
			m := NewNotifyWorker(tt.args.name, tt.args.mode, tt.args.taskFunc, tt.args.alertFunc)
			m.Start()

			var wg sync.WaitGroup
			wg.Add(tt.args.batchSize)

			for i := 0; i < tt.args.batchSize; i++ {
				go func() {
					defer wg.Done()
					m.NotifyRunTask()
					second := rand.New(rand.NewSource(time.Now().UnixNano())).Intn(3)
					time.Sleep(time.Second * time.Duration(second))
				}()
			}

			wg.Wait()                              // 等待所有通知任务完成
			time.Sleep(tt.args.waitResultDuration) // 等待任务执行完成

			got := n.Load()
			assert.Equal(t, tt.want, got)
		})
	}
}
