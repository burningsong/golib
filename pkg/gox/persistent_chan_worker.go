package gox

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitee.com/burningsong/golib/pkg/errx"
	"github.com/zeromicro/go-zero/core/logx"
	"golang.org/x/sync/semaphore"
)

// 异步任务worker
// 可持续运行,自我监控，异常退出后可自启
// 通过chan传递数据
// 可指定时间周期性执行任务(如间隔一定时间保存一次数据,间隔一段时间拉取一次任务等)

var defaultWorkerOptions = WorkerOptions{
	CronDuration: time.Hour * 24,
	AlertFunc: func(title, detail string) {
		logx.Errorf("%s, %s", title, detail)
	},
}

type WorkerOptions struct {
	FinishFunc    func()
	CronDuration  time.Duration
	CronFunc      func()
	AlertFunc     func(title, detail string)
	workerLimiter *semaphore.Weighted
}

type WorkerOption func(*WorkerOptions)

// WithFinishFunc 退出函数
func WithFinishFunc(finishFunc func()) WorkerOption {
	return func(o *WorkerOptions) {
		o.FinishFunc = finishFunc
	}
}

// WithCronFunc 定时执行函数
func WithCronFunc(cronDuration time.Duration, cronFunc func()) WorkerOption {
	return func(o *WorkerOptions) {
		o.CronDuration = cronDuration
		o.CronFunc = cronFunc
	}
}

// WithAlertFunc 报警函数
func WithAlertFunc(alert func(title, detail string)) WorkerOption {
	return func(o *WorkerOptions) {
		o.AlertFunc = alert
	}
}

// WithMaxConcurrency 处理Item数据的最大并发数
func WithMaxConcurrency(maxConcurrency int64) WorkerOption {
	return func(o *WorkerOptions) {
		o.workerLimiter = semaphore.NewWeighted(maxConcurrency)
	}
}

func NewPersistentChanWorker[T any](name string, dataChan chan T, handleItemFunc func(T), opts ...WorkerOption) *PersistentChanWorker[T] {
	options := defaultWorkerOptions
	for _, o := range opts {
		o(&options)
	}

	worker := &PersistentChanWorker[T]{
		name:              name,
		DataChan:          dataChan,
		keepAliveChan:     make(chan struct{}),
		shutdownSignal:    make(chan os.Signal, 1),
		isShutdown:        false,
		stopKeepAliveChan: make(chan struct{}),
		handleItemFunc:    handleItemFunc,
		finishFunc:        options.FinishFunc,
		cronDuration:      options.CronDuration,
		cronFunc:          options.CronFunc,
		alertFunc:         options.AlertFunc,
		workerLimiter:     options.workerLimiter,
	}
	return worker
}

type PersistentChanWorker[T any] struct {
	name string
	// 数据chan
	DataChan chan T
	// 保活，存储协程意外退出
	keepAliveChan  chan struct{}
	shutdownSignal chan os.Signal
	// 程序收到退出信号
	isShutdown bool
	// 停止保活
	stopKeepAliveChan chan struct{}
	// 处理函数
	handleItemFunc func(T)
	// 处理前执行函数
	beforeHandleItemFunc func(T)
	// 并发控制器
	workerLimiter *semaphore.Weighted
	// 退出函数
	finishFunc func()
	// 定时执行函数
	cronFunc func()
	// 定时执行函数的间隔
	cronDuration time.Duration
	// 报警函数
	alertFunc func(title, detail string)
}

func (w *PersistentChanWorker[T]) SetBeforeHandleItemFunc(beforeHandleItemFunc func(T)) {
	w.beforeHandleItemFunc = beforeHandleItemFunc
}

func (w *PersistentChanWorker[T]) alert(title, detail string) {
	if w.alertFunc != nil {
		w.alertFunc(title, detail)
	}
}

func (w *PersistentChanWorker[T]) runWithCron() {
	go func() {
		ticker := time.NewTicker(w.cronDuration)
		defer func() {
			if err := recover(); err != nil {
				w.alert(fmt.Sprintf("%s worker run recover", w.name), fmt.Sprintf("%s PersistentChanWorker run recover error:%+v,msg:%s", w.name, err, errx.GetStack()))
			}
			ticker.Stop()
			if !w.isShutdown {
				time.Sleep(time.Second)
				w.keepAliveChan <- struct{}{}
			}
		}()

		for {
			if w.checkShutdown() {
				return
			}

			select {
			case <-ticker.C:
				w.cronFunc()
				continue
			default:
			}

			select {
			case info := <-w.DataChan:
				w._handleItem(info)
			case <-ticker.C:
				w.cronFunc()
			case <-w.shutdownSignal:
				w.finishFunc()
				w.isShutdown = true
				w.stopKeepAliveChan <- struct{}{}
				return
			}
		}
	}()
}

func (w *PersistentChanWorker[T]) run() {
	go func() {
		defer func() {
			if err := recover(); err != nil {
				w.alert(fmt.Sprintf("%s worker run recover", w.name), fmt.Sprintf("%s PersistentChanWorker run recover error:%+v,msg:%s", w.name, err, errx.GetStack()))
			}
			if !w.isShutdown {
				time.Sleep(time.Second)
				w.keepAliveChan <- struct{}{}
			}
		}()

		for {
			if w.checkShutdown() {
				return
			}
			select {
			case info := <-w.DataChan:
				w._handleItem(info)
			case <-w.shutdownSignal:
				w.finishFunc()
				w.isShutdown = true
				w.stopKeepAliveChan <- struct{}{}
				return
			}
		}
	}()
}

func (w PersistentChanWorker[T]) checkShutdown() bool {
	select {
	case <-w.shutdownSignal:
		w.finishFunc()
		w.isShutdown = true
		w.stopKeepAliveChan <- struct{}{}
		return true
	default:
		return false
	}
}

func (w *PersistentChanWorker[T]) AddItem(info T) {
	w.DataChan <- info
}

// _handleItem 处理单个数据（命名避免被外部覆盖）
func (w *PersistentChanWorker[T]) _handleItem(info T) {
	defer func() {
		if err := recover(); err != nil {
			w.alert(fmt.Sprintf("%s worker _handleItem recover", w.name),
				fmt.Sprintf("%s _handleItem recover error:%+v,msg:%s info:%+v", w.name, err, errx.GetStack(), info))
		}
	}()

	if w.beforeHandleItemFunc != nil {
		w.beforeHandleItemFunc(info)
	}

	if w.workerLimiter == nil {
		w.handleItemFunc(info)
		return
	}

	w.workerLimiter.Acquire(context.Background(), 1)
	go func() {
		defer func() {
			if err := recover(); err != nil {
				w.alert(fmt.Sprintf("%s worker _handleItem recover", w.name),
					fmt.Sprintf("%s _handleItem recover error:%+v,msg:%s item:%+v", w.name, err, errx.GetStack(), info))
			}
			w.workerLimiter.Release(1)
		}()
		w.handleItemFunc(info)
	}()
}

func (w *PersistentChanWorker[T]) Start() {
	signal.Notify(w.shutdownSignal, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)
	w.keepAlive()
	if w.cronFunc != nil {
		w.runWithCron()
	} else {
		w.run()
	}
}

func (w *PersistentChanWorker[T]) keepAlive() {
	go func() {
		defer func() {
			if err := recover(); err != nil {
				w.alert(fmt.Sprintf("%s worker run recover", w.name),
					fmt.Sprintf("%s PersistentChanWorker keepAlive recover error:%+v,msg:%s", w.name, err, errx.GetStack()))
			}
		}()

		for {
			select {
			case <-w.keepAliveChan:
				w.run()
			case <-w.stopKeepAliveChan:
				w.alert(fmt.Sprintf("%s worker run closed", w.name),
					fmt.Sprintf("%s PersistentChanWorker keepAlive exit", w.name))
				return
			}
		}
	}()
}

// 主动停止
func (w *PersistentChanWorker[T]) Stop() {
	w.shutdownSignal <- syscall.SIGHUP
}
