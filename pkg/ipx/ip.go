package ipx

import "net"

// IsPrivateIP 判断是否为私有 IP
func IsPrivateIP(ip net.IP) bool {
	privateIPBlocks := []*net.IPNet{
		// 10.0.0.0/8
		&net.IPNet{
			IP:   net.IPv4(10, 0, 0, 0),
			Mask: net.CIDRMask(8, 32),
		},
		// 172.16.0.0/12
		&net.IPNet{
			IP:   net.IPv4(172, 16, 0, 0),
			Mask: net.CIDRMask(12, 32),
		},
		// 192.168.0.0/16
		&net.IPNet{
			IP:   net.IPv4(192, 168, 0, 0),
			Mask: net.CIDRMask(16, 32),
		},
		// 127.0.0.0/8 (Loopback address)
		&net.IPNet{
			IP:   net.IPv4(127, 0, 0, 0),
			Mask: net.CIDRMask(8, 32),
		},
		// 169.254.0.0/16 (Link-local address)
		&net.IPNet{
			IP:   net.IPv4(169, 254, 0, 0),
			Mask: net.CIDRMask(16, 32),
		},
	}

	for _, block := range privateIPBlocks {
		if block.Contains(ip) {
			return true
		}
	}
	return false
}

// IsPublicIP 判断是否为公网 IP
func IsPublicIP(ip net.IP) bool {
	return !IsPrivateIP(ip) && !ip.IsLoopback() && !ip.IsLinkLocalUnicast() && !ip.IsMulticast()
}
