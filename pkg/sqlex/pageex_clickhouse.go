package sqlex

func (p *Page) ClickHouseOrderBy() string {
	size := len(p.SortBy)
	if size == 0 {
		return ""
	}

	order := "order by "
	for i, v := range p.SortBy {
		order = order + v.OrderBy()
		if size-1 != i {
			order = order + ","
		}
	}
	return order
}

// ClickHouseOrderByExt 返回没有order by 前缀
func (p *Page) ClickHouseOrderByExt() string {
	size := len(p.SortBy)
	if size == 0 {
		return ""
	}

	order := ""
	for i, v := range p.SortBy {
		order = order + v.OrderBy()
		if size-1 != i {
			order = order + ","
		}
	}
	return order
}
