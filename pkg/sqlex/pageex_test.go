package sqlex

import "testing"

func TestPage_OrderBy(t *testing.T) {
	type fields struct {
		PageNo     int64
		PageSize   int64
		StartTime  int64
		EndTime    int64
		SortBy     []OrderItem
		IgnoreStat bool
		IgnoreList bool
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name: "empty",
			fields: fields{
				SortBy: nil,
			},
			want: "order by id DESC",
		},
		{
			name: "1 asc",
			fields: fields{
				SortBy: []OrderItem{{Column: "id", Asc: true}},
			},
			want: "order by id ASC",
		},
		{
			name: "2",
			fields: fields{
				SortBy: []OrderItem{{Column: "id", Asc: true}, {Column: "name", Asc: false}},
			},
			want: "order by id ASC,name DESC",
		},
		{
			name: "2 with custom order",
			fields: fields{
				SortBy: []OrderItem{{Column: "id", Asc: true}, {Column: "name", CustomOrderValues: []string{"a", "b"}}},
			},
			want: "order by id ASC, CASE WHEN name = 'a' THEN 0 WHEN name = 'b' THEN 1 ELSE 2 END ASC",
		},
		{
			name: "3 with custom order",
			fields: fields{
				SortBy: []OrderItem{{Column: "id", Asc: true}, {Column: "name", CustomOrderValues: []string{"a", "b"}}, {Column: "age", Asc: true}},
			},
			want: "order by id ASC, CASE WHEN name = 'a' THEN 0 WHEN name = 'b' THEN 1 ELSE 2 END ASC,age ASC",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &Page{
				PageNo:     tt.fields.PageNo,
				PageSize:   tt.fields.PageSize,
				StartTime:  tt.fields.StartTime,
				EndTime:    tt.fields.EndTime,
				SortBy:     tt.fields.SortBy,
				IgnoreStat: tt.fields.IgnoreStat,
				IgnoreList: tt.fields.IgnoreList,
			}
			if got := p.OrderBy(); got != tt.want {
				t.Errorf("Page.OrderBy() = %v, want %v", got, tt.want)
			}
		})
	}
}
