package grafanax

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDashboardManager_GetDashboard(t *testing.T) {
	config := Config{
		Url:          "https://127.0.0.1:2053",
		AccountToken: "glsa_GWqYPXN3XsH9uX342mhgnH9e3CIfcvyx_116c24cc",
	}
	uid := "c88b2fdf-8858-41f6-9ebc-526134a1c299"
	m := NewDashboardManager(config)
	dc, err := m.GetDashboard(uid)
	assert.NoError(t, err)

	t.Log(dc)

}

func TestDashboardManager_UpdateDashboard(t *testing.T) {

	config := Config{
		Url:          "https://127.0.0.1:2053",
		AccountToken: "glsa_GWqYPXN3XsH9uX342mhgnH9e3CIfcvyx_116c24cc",
	}
	uid := "c88b2fdf-8858-41f6-9ebc-526134a1c299"
	m := NewDashboardManager(config)
	dc, err := m.GetDashboard(uid)
	assert.NoError(t, err)
	assert.False(t, len(dc.Panels) == 0)

	// newPannel := dc.Panels[0]

}

func TestDashboardManager_SearchDashboards(t *testing.T) {
	config := Config{
		Url:          "https://127.0.0.1:2053",
		AccountToken: "glsa_GWqYPXN3XsH9uX342mhgnH9e3CIfcvyx_116c24cc",
	}
	m := NewDashboardManager(config)
	req := DashboardSearchReq{
		Query:         "",
		Tag:           []string{},
		Type:          "",
		DashboardIds:  []int{22, 24},
		DashboardUIDs: []string{},
		FolderIds:     []int{25},
		Starred:       false,
		Limit:         0,
		Page:          0,
	}
	d, err := m.SearchDashboards(context.TODO(), req)
	assert.NoError(t, err)
	t.Log(d)

}
