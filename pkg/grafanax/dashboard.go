package grafanax

type Dashboard struct {
	Annotations          Annotations   `json:"annotations"`
	Editable             bool          `json:"editable"`
	FiscalYearStartMonth int           `json:"fiscalYearStartMonth"`
	GraphTooltip         int           `json:"graphTooltip"`
	ID                   int           `json:"id"`
	Links                []interface{} `json:"links"`
	LiveNow              bool          `json:"liveNow"`
	Panels               []Panel       `json:"panels"`
	Refresh              string        `json:"refresh"`
	SchemaVersion        int           `json:"schemaVersion"`
	Tags                 []string      `json:"tags"`
	Templating           Templating    `json:"templating"`
	Time                 Time          `json:"time"`
	Timepicker           struct{}      `json:"timepicker"`
	Timezone             string        `json:"timezone"`
	Title                string        `json:"title"`
	UID                  string        `json:"uid"`
	Version              int           `json:"version"`
	WeekStart            string        `json:"weekStart"`
	URI                  string        `json:"uri"` //list append
	URL                  string        `json:"url"`
	Slug                 string        `json:"slug"`
	Type                 string        `json:"type"`
	IsStarred            bool          `json:"isStarred"`
	FolderID             int           `json:"folderId"`
	FolderUID            string        `json:"folderUid"`
	FolderTitle          string        `json:"folderTitle"`
	FolderURL            string        `json:"folderUrl"`
	SortMeta             int           `json:"sortMeta"`
}

type Annotations struct {
	List []Annotation `json:"list"`
}

type Annotation struct {
	BuiltIn    int        `json:"builtIn"`
	DataSource DataSource `json:"datasource"`
	Enable     bool       `json:"enable"`
	Hide       bool       `json:"hide"`
	IconColor  string     `json:"iconColor"`
	Name       string     `json:"name"`
	Type       string     `json:"type"`
}

type Templating struct {
	List []interface{} `json:"list"`
}

type Time struct {
	From string `json:"from"`
	To   string `json:"to"`
}
