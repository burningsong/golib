package grafanax

type Panel struct {
	DataSource      DataSource       `json:"datasource"`
	Description     string           `json:"description"`
	FieldConfig     FieldConfig      `json:"fieldConfig"`
	GridPos         GridPos          `json:"gridPos"`
	ID              int              `json:"id"`
	MaxDataPoints   int              `json:"maxDataPoints"`
	Options         Options          `json:"options"`
	Targets         []Target         `json:"targets"` //query的线
	Title           string           `json:"title"`
	Transformations []Transformation `json:"transformations"`
	Type            string           `json:"type"`
}

type DataSource struct {
	Type string `json:"type"`
	UID  string `json:"uid"`
}

type FieldConfig struct {
	Defaults  Defaults   `json:"defaults"`
	Overrides []Override `json:"overrides"`
}

type GridPos struct {
	H int `json:"h"`
	W int `json:"w"`
	X int `json:"x"`
	Y int `json:"y"`
}

type Defaults struct {
	Color      ColorMap   `json:"color"`
	Custom     Custom     `json:"custom"`
	Mappings   []Mapping  `json:"mappings"`
	Thresholds Thresholds `json:"thresholds"`
	Unit       string     `json:"unit"`
}

type ColorMap struct {
	Mode string `json:"mode"`
}

type Custom struct {
	AxisBorderShow    bool              `json:"axisBorderShow"`
	AxisCenteredZero  bool              `json:"axisCenteredZero"`
	AxisColorMode     string            `json:"axisColorMode"`
	AxisLabel         string            `json:"axisLabel"`
	AxisPlacement     string            `json:"axisPlacement"`
	BarAlignment      int               `json:"barAlignment"`
	DrawStyle         string            `json:"drawStyle"`
	FillOpacity       int               `json:"fillOpacity"`
	GradientMode      string            `json:"gradientMode"`
	HideFrom          HideFrom          `json:"hideFrom"`
	InsertNulls       bool              `json:"insertNulls"`
	LineInterpolation string            `json:"lineInterpolation"`
	LineWidth         int               `json:"lineWidth"`
	PointSize         int               `json:"pointSize"`
	ScaleDistribution ScaleDistribution `json:"scaleDistribution"`
	ShowPoints        string            `json:"showPoints"`
	SpanNulls         bool              `json:"spanNulls"`
	Stacking          Stacking          `json:"stacking"`
	ThresholdsStyle   ThresholdsStyle   `json:"thresholdsStyle"`
}

type HideFrom struct {
	Legend  bool `json:"legend"`
	Tooltip bool `json:"tooltip"`
	Viz     bool `json:"viz"`
}

type ScaleDistribution struct {
	Type string `json:"type"`
}

type Stacking struct {
	Group string `json:"group"`
	Mode  string `json:"mode"`
}

type ThresholdsStyle struct {
	Mode string `json:"mode"`
}

type Thresholds struct {
	Mode  string `json:"mode"`
	Steps []Step `json:"steps"`
}

type Step struct {
	Color string      `json:"color"`
	Value interface{} `json:"value"`
}

type Mapping struct {
	// 定义 Mapping 相关的字段
}

type Override struct {
	// 定义 Override 相关的字段
}

type Options struct {
	Legend  Legend  `json:"legend"`
	Tooltip Tooltip `json:"tooltip"`
}

type Legend struct {
	Calcs       []string `json:"calcs"`
	DisplayMode string   `json:"displayMode"`
	Placement   string   `json:"placement"`
	ShowLegend  bool     `json:"showLegend"`
}

type Tooltip struct {
	Mode string `json:"mode"`
	Sort string `json:"sort"`
}

type Target struct {
	Alias      string       `json:"alias"`
	BucketAggs []BucketAggs `json:"bucketAggs"` //
	DataSource DataSource   `json:"datasource"`
	Metrics    []Metric     `json:"metrics"` //
	Query      string       `json:"query"`   //查询语句"success: true AND groupMemberLevel: $groupMemberLevel AND groupName: $groupName"
	RefID      string       `json:"refId"`
	TimeField  string       `json:"timeField"`
	Hide       bool         `json:"hide,omitempty"`
}

type BucketAggs struct {
	Field    string   `json:"field"`
	ID       string   `json:"id"`
	Settings Settings `json:"settings"`
	Type     string   `json:"type"`
}

type Settings struct {
	Interval    string `json:"interval"`
	MinDocCount string `json:"min_doc_count,omitempty"`
	TimeZone    string `json:"timeZone,omitempty"`
}

type Metric struct {
	ID   string `json:"id"`
	Type string `json:"type"`
}

type Transformation struct {
	ID      string       `json:"id"`
	Options TransOptions `json:"options"`
}

type TransOptions struct {
	Binary        Binary `json:"binary"`
	Mode          string `json:"mode"`
	Reduce        Reduce `json:"reduce"`
	ReplaceFields bool   `json:"replaceFields"`
}

type Binary struct {
	Left     string `json:"left"`
	Operator string `json:"operator"`
	Right    string `json:"right"`
}

type Reduce struct {
	Reducer string `json:"reducer"`
}
