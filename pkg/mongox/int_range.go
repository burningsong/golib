package mongox

import "go.mongodb.org/mongo-driver/bson"

type IntRange[T int | int64 | int32] struct {
	Min *T `json:"min,optional"`
	Max *T `json:"max,optional"`
}

func (i *IntRange[T]) FilterM() bson.M {
	if i.Min != nil && i.Max != nil && *i.Min == *i.Max {
		return bson.M{"$eq": *i.Min}
	}

	filter := bson.M{}
	if i.Min != nil {
		filter["$gte"] = *i.Min
	}
	if i.Max != nil {
		filter["$lt"] = *i.Max
	}
	return filter
}

func (i *IntRange[T]) FilterD() bson.D {
	if i.Min != nil && i.Max != nil && *i.Min == *i.Max {
		return bson.D{{"$eq", *i.Min}}
	}

	filter := bson.D{}
	if i.Min != nil {
		filter = append(filter, bson.E{"$gte", *i.Min})
	}
	if i.Max != nil {
		filter = append(filter, bson.E{"$lt", *i.Max})
	}
	return filter
}
