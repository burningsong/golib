package stringx

import (
	"reflect"
	"testing"
)

func TestSplitByMultiSep(t *testing.T) {
	type args struct {
		s                 string
		separators        []string
		ignoreEmptyString bool
	}
	tests := []struct {
		name string
		args args
		want []string
	}{
		{
			name: "test1",
			args: args{
				s:                 "a:b,c,d，e",
				separators:        []string{":", ",", "，"},
				ignoreEmptyString: true,
			},
			want: []string{"a", "b", "c", "d", "e"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := SplitByMultiSep(tt.args.s, tt.args.separators, tt.args.ignoreEmptyString); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("SplitByMultiSep() = %v, want %v", got, tt.want)
			}
		})
	}
}
