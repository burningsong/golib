package stringx

import (
	"strings"
)

// SplitByMultiSep 按照多个分隔符分割字符串
func SplitByMultiSep(s string, separators []string, ignoreEmptyString bool) []string {
	// 如果分隔符数组为空，直接返回原字符串
	if len(separators) == 0 {
		return []string{s}
	}

	// 使用第一个分隔符进行初始分割
	result := strings.Split(s, separators[0])

	// 对于剩余的每个分隔符，继续分割所有子字符串
	for i := 1; i < len(separators); i++ {
		var newResult []string
		for _, str := range result {
			// 对每个子字符串使用当前分隔符进行分割
			parts := strings.Split(str, separators[i])
			newResult = append(newResult, parts...)
		}
		result = newResult
	}

	if !ignoreEmptyString {
		return result
	}

	// 过滤空字符串
	var filtered []string
	for _, str := range result {
		if str != "" {
			filtered = append(filtered, str)
		}
	}

	return filtered
}
