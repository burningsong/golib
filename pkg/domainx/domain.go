package domainx

import (
	"fmt"
	"regexp"

	"github.com/weppos/publicsuffix-go/publicsuffix"
)

// GetMainDomain 获取主域名
func GetMainDomain(domain string) (string, error) {
	return publicsuffix.Domain(domain)
}

// MatchDomain 匹配域名
// 如：filter为example.com 可以匹配 example.com,sub.example.com,www.example.com
func MatchDomain(filter string, domain string) (bool, error) {
	// 使用 strings.Replace 转义 filter 中的特殊字符
	escapedFilter := regexp.QuoteMeta(filter)

	// 构建正则表达式：^(.*\.)?{escapedFilter}$
	regexPattern := fmt.Sprintf(`^(.*\.)?%s$`, escapedFilter)

	// 编译正则表达式
	re, err := regexp.Compile(regexPattern)
	if err != nil {
		return false, err
	}

	// 使用正则表达式匹配域名
	return re.MatchString(domain), nil
}
