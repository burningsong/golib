package logex

import (
	"context"

	"github.com/zeromicro/go-zero/core/logx"
)

// LogLevel 定义日志级别
type LogLevel int

const (
	LogDebug LogLevel = iota
	LogInfo
	LogWarn
	LogError
)

type RestyLogger struct {
	level  LogLevel
	logger logx.Logger
}

// NewRestyLogger 创建一个新的 RestyLogger 实例
func NewRestyLogger(ctx context.Context, level LogLevel) *RestyLogger {
	return &RestyLogger{
		logger: logx.WithContext(ctx),
		level:  level,
	}
}

// Errorf 实现 resty.Logger 接口的 Errorf 方法
func (l *RestyLogger) Errorf(format string, v ...interface{}) {
	if l.level <= LogError {
		l.logger.Errorf(format, v...)
	}
}

// Warnf 实现 resty.Logger 接口的 Warnf 方法
func (l *RestyLogger) Warnf(format string, v ...interface{}) {
	if l.level <= LogWarn {
		l.logger.Infof(format, v...)
	}
}

// Debugf 实现 resty.Logger 接口的 Debugf 方法
func (l *RestyLogger) Debugf(format string, v ...interface{}) {
	if l.level <= LogDebug {
		l.logger.Debugf(format, v...)
	}
}

// SetLevel 设置日志级别
func (l *RestyLogger) SetLevel(level LogLevel) {
	l.level = level
}
