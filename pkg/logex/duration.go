package logex

import (
	"time"

	"github.com/zeromicro/go-zero/core/logx"
)

func LogExecutionTime(name string, extra any) func() {
	logx.Infof("%s start %+v", name, extra)
	startTime := time.Now()
	return func() {
		logx.Infof("%s end cost %s %+v", name, time.Since(startTime).String(), extra)
	}
}
