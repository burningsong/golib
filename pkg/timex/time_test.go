package timex

import (
	"testing"
	"time"
)

func TestFormatChinaTime(t *testing.T) {
	type args struct {
		t      time.Time
		layout string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "Test local",
			args: args{
				t:      time.Now(),
				layout: "2006-01-02 15:04",
			},
			want: "2024-11-16 18:18", // local converted to Shanghai
		},
		{
			name: "Test UTC to Shanghai",
			args: args{
				t:      time.Date(2024, 11, 16, 15, 30, 0, 0, time.UTC),
				layout: "2006-01-02 15:04:05",
			},
			want: "2024-11-16 23:30:00", // Expected Shanghai time
		},
		{
			name: "Test Already in Shanghai",
			args: args{
				t:      time.Date(2024, 11, 16, 23, 30, 0, 0, time.FixedZone(ZoneShanghaiName, 8*3600)),
				layout: "2006-01-02 15:04:05",
			},
			want: "2024-11-16 23:30:00", // Already Shanghai time
		},
		{
			name: "Test New York to Shanghai",
			args: args{
				t:      time.Date(2024, 11, 16, 10, 30, 0, 0, time.FixedZone("America/New_York", -5*3600)),
				layout: "2006-01-02 15:04:05",
			},
			want: "2024-11-16 23:30:00", // New York converted to Shanghai
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := FormatChinaTime(tt.args.t, tt.args.layout); got != tt.want {
				t.Errorf("FormatChinaTime() = %v, want %v", got, tt.want)
			}
		})
	}
}
