package proxyx

import "time"

type (
	ProxyConfig struct {
		ProxyUrl  string
		Socks5Url string
		Username  string
		Password  string
		Url       string
	}

	ClientConfig struct {
		IgnoreCert        bool //是否忽略证书验证
		ForceAttemptHTTP2 bool //是否强制使用 HTTP2
		Timeout           time.Duration
	}
)
