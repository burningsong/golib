package errx

import (
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func CreateGrpcError(code int64, message string) error {
	return status.Errorf(codes.Code(code), message)
}

func ParseGrpcError(err error) (code int64, error string) {
	if err == nil {
		return 0,""
	}
	
	if gstatus, ok := status.FromError(err); ok { // grpc err错误
		grpcCode := int64(gstatus.Code())
		return grpcCode, gstatus.Message()
	}

	return 500, err.Error()
}
