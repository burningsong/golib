package slicex

import "testing"

func TestHasSameElementIgnoreOrder(t *testing.T) {
	type args struct {
		s1      []string
		s2      []string
		compare func(a, b string) bool
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "string same",
			args: args{
				s1: []string{"a", "b", "c"},
				s2: []string{"c", "a", "b"},
				compare: func(a, b string) bool {
					return a < b
				},
			},
			want: true,
		},
		{
			name: "string no same",
			args: args{
				s1: []string{"a", "b", "c"},
				s2: []string{"c", "a", "a"},
				compare: func(a, b string) bool {
					return a < b
				},
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := HasSameElementIgnoreOrder(tt.args.s1, tt.args.s2, tt.args.compare); got != tt.want {
				t.Errorf("HasSameElementIgnoreOrder() = %v, want %v", got, tt.want)
			}
		})
	}
}
