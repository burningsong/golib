package slicex

import (
	"slices"
	"sort"
)

// HasSameElementIgnoreOrder checks if two slices of any type have the same elements, ignoring order.
// It accepts a custom compare function to handle the comparison logic for different types.
func HasSameElementIgnoreOrder[T comparable](s1, s2 []T, compare func(a, b T) bool) bool {
	if len(s1) != len(s2) {
		return false
	}

	// Sort both slices using the provided compare function
	sort.Slice(s1, func(i, j int) bool {
		return compare(s1[i], s1[j])
	})
	sort.Slice(s2, func(i, j int) bool {
		return compare(s2[i], s2[j])
	})

	// Compare sorted slices
	return slices.Equal(s1, s2)
}

// HasIntersection checks if two slices of any type have any common elements.
func HasIntersection[T comparable](s1, s2 []T, equal func(a, b T) bool) bool {
	for _, v1 := range s1 {
		for _, v2 := range s2 {
			if equal(v1, v2) {
				return true
			}
		}
	}
	return false
}
