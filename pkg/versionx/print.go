package versionx

import (
	"cmp"
	"fmt"
	"os"
)

var (
	_version int = 1
)

// SetupVersion 设置版本信息，并且根据命令行标志设置版本信息
// versionFlag 命令行标志
func SetupVersion(version int, versionFlag string) {
	_version = version
	printVersion(cmp.Or(versionFlag, "--version"))
}

func printVersion(versionFlag string) {
	// 遍历命令行参数
	for _, arg := range os.Args {
		if arg == versionFlag {
			// 输出版本信息
			fmt.Printf("Version: %d\n", _version)
			os.Exit(0) // 输出后退出程序
		}
	}
}
