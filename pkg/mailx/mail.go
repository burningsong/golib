package mailx

import (
	"net/mail"
	"regexp"
	"strings"
)

func IsEmail(email string) bool {
	_, err := mail.ParseAddress(email)
	return err == nil
}

// IsValidEmail 使用正则表达式验证邮箱格式
func IsValidEmail(email string) bool {
	// 基本的邮箱正则表达式
	// 1. 用户名部分可以包含字母、数字、下划线、点、连字符
	// 2. @ 符号
	// 3. 域名部分包含字母、数字、连字符和点
	emailRegex := regexp.MustCompile(`^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$`)
	return emailRegex.MatchString(email)
}

// IsValidEmailDetailed 提供更详细的邮箱验证并返回错误原因
func IsValidEmailDetailed(email string) (bool, string) {
	// 1. 检查邮箱是否为空
	if email == "" {
		return false, "邮箱不能为空"
	}

	// 2. 检查邮箱长度
	if len(email) > 254 {
		return false, "邮箱长度不能超过254个字符"
	}

	// 3. 检查是否包含 @ 符号
	if !strings.Contains(email, "@") {
		return false, "邮箱必须包含 @ 符号"
	}

	// 4. 分割邮箱为用户名和域名部分
	parts := strings.Split(email, "@")
	if len(parts) != 2 {
		return false, "邮箱格式错误：必须只包含一个 @ 符号"
	}

	username, domain := parts[0], parts[1]

	// 5. 检查用户名部分
	if len(username) == 0 {
		return false, "用户名部分不能为空"
	}

	// 6. 检查域名部分
	if len(domain) == 0 {
		return false, "域名部分不能为空"
	}

	// 7. 检查域名是否包含至少一个点号
	if !strings.Contains(domain, ".") {
		return false, "域名必须包含至少一个点号"
	}

	// 8. 检查顶级域名
	domainParts := strings.Split(domain, ".")
	tld := domainParts[len(domainParts)-1]
	if len(tld) < 2 {
		return false, "顶级域名至少需要2个字符"
	}

	// 9. 使用正则表达式进行完整验证
	emailRegex := regexp.MustCompile(`^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$`)
	if !emailRegex.MatchString(email) {
		return false, "邮箱格式不符合规范"
	}

	return true, ""
}
