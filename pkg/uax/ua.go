package uax

type (
	UAGenerator interface {
		Generate() (string, error)
	}
)

func RandomUa() string {
	return _curlUAGenerator.Generate()
}
