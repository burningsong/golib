package filex

import (
	"io"
	"strings"

	"mime/multipart"
	"os"
	"path/filepath"

	"github.com/gogf/gf/crypto/gmd5"
)

// Exist 检查指定路径的文件或文件夹是否存在
// 返回 true 表示存在，false 表示不存在
// 如果无法确定，返回错误
func Exist(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil // 文件或目录存在
	}
	if os.IsNotExist(err) {
		return false, nil // 文件或目录不存在
	}
	return false, err // 发生了其他错误，无法确定
}

func CreateSavePath(dst string, perm os.FileMode) error {
	err := os.MkdirAll(dst, perm)
	if err != nil {
		return err
	}

	return nil
}

func SaveFile(file *multipart.FileHeader, dst string) error {
	src, err := file.Open()
	if err != nil {
		return err
	}
	defer src.Close()

	out, err := os.Create(dst)
	if err != nil {
		return err
	}
	defer out.Close()

	_, err = io.Copy(out, src)
	return err
}

func SaveStringToFile(data, dst string) error {
	reader := strings.NewReader(data)

	out, err := os.Create(dst)
	if err != nil {
		return err
	}
	defer out.Close()

	_, err = io.Copy(out, reader)
	return err
}

func GetFileHeaderMd5Name(fileHeader *multipart.FileHeader) (string, error) {
	file, err := fileHeader.Open()
	if err != nil {
		return "", err
	}

	body, err := io.ReadAll(file)
	if err != nil {
		return "", err
	}

	name, err := gmd5.EncryptBytes(body)
	if err != nil {
		return "", err
	}

	return name + filepath.Ext(fileHeader.Filename), nil
}

// GetFileNameWithoutExtension 获取文件名(不含后缀),如：dir1/dir2/test.txt，则返回test
func GetFileNameWithoutExtension(path string) string {
	// Extract the file name with extension
	fileName := filepath.Base(path)
	// Remove the extension
	return strings.TrimSuffix(fileName, filepath.Ext(fileName))
}
