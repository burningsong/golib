package sslx

import "testing"

func TestCheckSSLCertificate(t *testing.T) {
	type args struct {
		domain string
	}
	tests := []struct {
		name    string
		args    args
		want    bool
		wantErr bool
	}{
		{
			name: "test",
			args: args{
				domain: "testping.syncwithgoogle.com",
			},
			want:    true,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := CheckSSLCertificate(tt.args.domain)
			if (err != nil) != tt.wantErr {
				t.Errorf("CheckSSLCertificate() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("CheckSSLCertificate() = %v, want %v", got, tt.want)
			}
		})
	}
}
