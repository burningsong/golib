package syncx

import (
	"testing"
	"time"

	"math/rand"
)

func TestNewShardedLock(t *testing.T) {
	shardedLock := NewShardedLock(10) // 创建一个有10个分片的分片锁

	shardID := 5 // 假设我们要访问的数据位于第5个分片
	shardedLock.Lock(shardID)

	// 访问和修改位于第5个分片的数据

	shardedLock.Unlock(shardID)
}

func TestShardedLock_HashShard(t *testing.T) {
	locker := NewShardedLock(20)

	keys := []string{"6736d1704f9eb71781768568", "673714b34f9eb717817685d1"}

	for _, v := range keys {
		val := locker.GetHashShard(v)
		t.Log(val)
	}

	t.Log("-------v2---------")
	for _, v := range keys {
		val := locker.GetHashShardV2(v)
		t.Log(val)
	}
}

// 随机生成字符串
func randomString(n int) string {
	letters := []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
	result := make([]rune, n)
	src := rand.NewSource(time.Now().UnixNano())
	r := rand.New(src)
	for i := range result {
		result[i] = letters[r.Intn(len(letters))]
	}
	return string(result)
}

// 计算方差
func calculateVariance(counts map[int]int, totalKeys int) float64 {
	shardCount := len(counts)
	mean := float64(totalKeys) / float64(shardCount)

	var variance float64
	for _, count := range counts {
		diff := float64(count) - mean
		variance += diff * diff
	}
	return variance / float64(shardCount)
}

// 测试 ShardedLock 的 HashShard 方法分布
func TestShardedLock_HashShardRandom(t *testing.T) {
	src := rand.NewSource(time.Now().UnixNano())
	r := rand.New(src)

	// 控制测试轮数
	rounds := 200
	shardWins := 0
	shardV2Wins := 0
	draws := 0

	for i := 0; i < rounds; i++ {
		// 随机生成 shardCount 和 keyCount
		shardCount := r.Intn(50) + 50   // 随机分片数 50 ~ 100
		keyCount := r.Intn(9000) + 1000 // 随机键数 1000 ~ 10000

		locker := NewShardedLock(shardCount)
		keys := make([]string, 0, keyCount)

		// 随机生成 keys
		for i := 0; i < cap(keys); i++ {
			keys = append(keys, randomString(10))
		}

		// 统计 GetHashShard 分布
		countsV1 := make(map[int]int)
		for _, v := range keys {
			val := locker.GetHashShard(v)
			countsV1[val]++
		}
		varianceV1 := calculateVariance(countsV1, len(keys))

		// 统计 GetHashShardV2 分布
		countsV2 := make(map[int]int)
		for _, v := range keys {
			val := locker.GetHashShardV2(v)
			countsV2[val]++
		}
		varianceV2 := calculateVariance(countsV2, len(keys))

		// 比较方差
		if varianceV1 < varianceV2 {
			shardWins++
		} else if varianceV1 > varianceV2 {
			shardV2Wins++
		} else {
			draws++
		}
	}

	// 输出统计结果
	t.Logf("测试轮数: %d", rounds)
	t.Logf("GetHashShard 胜出次数: %d", shardWins)
	t.Logf("GetHashShardV2 胜出次数: %d", shardV2Wins)
	t.Logf("平局次数: %d", draws)
}
