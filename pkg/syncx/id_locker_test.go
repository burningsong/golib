package syncx

import (
	"sync"
	"sync/atomic"
	"testing"
	"time"
)

func TestIDLockerBasicLockUnlock(t *testing.T) {
	locker := NewIDLocker()
	defer locker.Close()

	// 简单的锁定和解锁
	locker.Lock("test1")
	locker.Unlock("test1")

	// 重复锁定和解锁
	locker.Lock("test2")
	locker.Lock("test2") // 应该被阻塞，直到解锁
	locker.Unlock("test2")
	locker.Unlock("test2")

	// 解锁不存在的ID不应该导致异常
	locker.Unlock("nonexistent")
}

func TestIDLockerConcurrency(t *testing.T) {
	locker := NewIDLocker()
	defer locker.Close()

	const (
		numGoroutines = 100
		numIterations = 50
	)

	// 测试多个goroutine对同一个ID的并发加锁
	sharedCounter := 0
	var wg sync.WaitGroup

	for i := 0; i < numGoroutines; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			for j := 0; j < numIterations; j++ {
				locker.Lock("shared")
				// 临界区
				sharedCounter++
				locker.Unlock("shared")
			}
		}()
	}

	wg.Wait()
	if sharedCounter != numGoroutines*numIterations {
		t.Errorf("预期计数器值为 %d, 实际为 %d", numGoroutines*numIterations, sharedCounter)
	}
}

func TestIDLockerTryLock(t *testing.T) {
	locker := NewIDLocker()
	defer locker.Close()

	// 测试TryLock在锁可用时的行为
	if !locker.TryLock("trylock1") {
		t.Error("TryLock应该在锁可用时返回true")
	}
	locker.Unlock("trylock1")

	// 测试TryLock在锁不可用时的行为
	locker.Lock("trylock2")
	if locker.TryLock("trylock2") {
		t.Error("TryLock应该在锁不可用时返回false")
	}
	locker.Unlock("trylock2")

	// 测试从不同goroutine进行TryLock
	locker.Lock("trylock3")

	var success atomic.Bool
	go func() {
		if locker.TryLock("trylock3") {
			success.Store(true)
			locker.Unlock("trylock3")
		}
	}()

	time.Sleep(100 * time.Millisecond)
	if success.Load() {
		t.Error("TryLock不应该在锁被其他goroutine持有时成功")
	}

	locker.Unlock("trylock3")
	time.Sleep(100 * time.Millisecond)

	// 现在应该可以获取锁
	if !locker.TryLock("trylock3") {
		t.Error("TryLock应该在锁被释放后成功")
	}
	locker.Unlock("trylock3")
}

func TestIDLockerLockWithTimeout(t *testing.T) {
	locker := NewIDLocker()
	defer locker.Close()

	// 测试在锁可用时立即成功
	if !locker.LockWithTimeout("timeout1", 1*time.Second) {
		t.Error("LockWithTimeout应该在锁可用时立即成功")
	}
	locker.Unlock("timeout1")

	// 测试超时情况
	locker.Lock("timeout2")

	start := time.Now()
	success := locker.LockWithTimeout("timeout2", 500*time.Millisecond)
	elapsed := time.Since(start)

	if success {
		t.Error("LockWithTimeout应该在超时时返回false")
	}

	if elapsed < 500*time.Millisecond {
		t.Error("LockWithTimeout似乎没有等待足够的时间")
	}

	locker.Unlock("timeout2")

	// 测试在等待期间锁被释放的情况
	locker.Lock("timeout3")

	go func() {
		time.Sleep(250 * time.Millisecond)
		locker.Unlock("timeout3")
	}()

	start = time.Now()
	success = locker.LockWithTimeout("timeout3", 1*time.Second)
	elapsed = time.Since(start)

	if !success {
		t.Error("LockWithTimeout应该在锁在超时前被释放时成功")
	}

	if elapsed < 250*time.Millisecond {
		t.Error("LockWithTimeout似乎太早成功")
	}

	if elapsed >= 1*time.Second {
		t.Error("LockWithTimeout似乎等待了完整的超时时间，尽管锁在期间被释放")
	}

	locker.Unlock("timeout3")
}

func TestIDLockerDifferentIDs(t *testing.T) {
	locker := NewIDLocker()
	defer locker.Close()

	// 测试不同ID的锁相互不影响
	locker.Lock("id1")

	var wg sync.WaitGroup
	wg.Add(1)

	go func() {
		defer wg.Done()
		// 这应该能立即成功，因为是不同的ID
		locker.Lock("id2")
		time.Sleep(100 * time.Millisecond)
		locker.Unlock("id2")
	}()

	wg.Wait() // 如果上面的goroutine阻塞，测试将挂起
	locker.Unlock("id1")
}

func TestIDLockerCleanup(t *testing.T) {
	// 创建一个清理间隔非常短的IDLocker用于测试
	locker := NewIDLockerWithExpiration(500 * time.Millisecond)
	defer locker.Close()

	// 获取内部lockMap的大小（通过反射或导出方法）
	getLocksCount := func() int {
		count := 0
		for range locker.locks.IterBuffered() {
			count++
		}
		return count
	}

	// 创建一些锁然后立即解锁
	for i := 0; i < 10; i++ {
		id := "cleanup-test-" + string(rune('a'+i))
		locker.Lock(id)
		locker.Unlock(id)
	}

	// 确认创建了锁
	initialCount := getLocksCount()
	if initialCount == 0 {
		t.Error("应该创建了一些锁")
	}

	// 等待清理发生
	time.Sleep(2 * time.Second)

	// 验证锁被清理
	finalCount := getLocksCount()
	if finalCount >= initialCount {
		t.Errorf("应该清理了一些锁，初始数量: %d, 最终数量: %d", initialCount, finalCount)
	}
}

func TestIDLockerRefCounting(t *testing.T) {
	locker := NewIDLockerWithExpiration(500 * time.Millisecond)
	defer locker.Close()

	// 获取内部lockMap的大小
	getLocksCount := func() int {
		count := 0
		for range locker.locks.IterBuffered() {
			count++
		}
		return count
	}

	testID := "refcount-test"

	// 多次锁定同一个ID，模拟多个引用
	locker.Lock(testID)
	locker.Lock(testID)
	locker.Lock(testID)

	// 解锁两次，保持一个引用
	locker.Unlock(testID)
	locker.Unlock(testID)

	// 等待清理周期
	time.Sleep(1 * time.Second)

	// 检查锁是否仍然存在（应该存在，因为还有引用）
	if getLocksCount() == 0 {
		t.Error("锁不应该被清理，因为还有一个引用")
	}

	// 解锁最后一个引用
	locker.Unlock(testID)

	// 再次等待清理周期
	time.Sleep(1 * time.Second)

	// 检查锁是否被清理
	if getLocksCount() > 0 {
		t.Error("锁应该在最后一个引用释放后被清理")
	}
}

func BenchmarkIDLockerLockUnlock(b *testing.B) {
	locker := NewIDLocker()
	defer locker.Close()

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		locker.Lock("bench-id")
		locker.Unlock("bench-id")
	}
}

func BenchmarkIDLockerConcurrentDifferentIDs(b *testing.B) {
	locker := NewIDLocker()
	defer locker.Close()

	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		// 每个并行协程使用不同的ID
		localID := "bench-id-" + time.Now().String()
		for pb.Next() {
			locker.Lock(localID)
			locker.Unlock(localID)
		}
	})
}

func BenchmarkIDLockerConcurrentSameID(b *testing.B) {
	locker := NewIDLocker()
	defer locker.Close()

	const sharedID = "bench-shared-id"

	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		// 所有并行协程使用相同的ID
		for pb.Next() {
			locker.Lock(sharedID)
			locker.Unlock(sharedID)
		}
	})
}
