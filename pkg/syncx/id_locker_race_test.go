package syncx

import (
	"runtime"
	"sync"
	"testing"
	"time"
)

// TestRaceConditions 使用竞态检测器（Go Race Detector）测试IDLocker
// 运行: go test -race -run=TestRaceConditions
func TestRaceConditions(t *testing.T) {
	// 使用更短的清理时间加速测试
	locker := NewIDLockerWithExpiration(200 * time.Millisecond)
	defer locker.Close()

	const (
		numIDs        = 20  // 使用的ID数量
		numGoroutines = 100 // 并发goroutine数量
		numOperations = 50  // 每个goroutine执行的操作数
	)

	// 生成测试使用的ID
	ids := make([]string, numIDs)
	for i := range ids {
		ids[i] = "race-test-" + string(rune('a'+i))
	}

	// 运行并发测试
	var wg sync.WaitGroup
	for g := 0; g < numGoroutines; g++ {
		wg.Add(1)
		go func(gNum int) {
			defer wg.Done()

			for i := 0; i < numOperations; i++ {
				// 选择一个ID
				idIndex := (gNum + i) % numIDs
				id := ids[idIndex]

				// 随机选择一个操作
				// 使用gNum和i的组合来伪随机选择操作
				op := (gNum * i) % 5

				switch op {
				case 0:
					// 基本的锁定和解锁
					locker.Lock(id)
					runtime.Gosched() // 增加竞争的可能性
					locker.Unlock(id)

				case 1:
					// 尝试锁定
					if locker.TryLock(id) {
						runtime.Gosched()
						locker.Unlock(id)
					}

				case 2:
					// 短超时锁定
					if locker.LockWithTimeout(id, 5*time.Millisecond) {
						runtime.Gosched()
						locker.Unlock(id)
					}

				case 3:
					// 双重锁定（同一个goroutine持有多个锁）
					locker.Lock(id)
					runtime.Gosched()
					// 获取另一个ID的锁
					otherID := ids[(idIndex+1)%numIDs]
					locker.Lock(otherID)
					runtime.Gosched()
					locker.Unlock(otherID)
					locker.Unlock(id)

				case 4:
					// 长超时锁定
					if locker.LockWithTimeout(id, 20*time.Millisecond) {
						runtime.Gosched()
						locker.Unlock(id)
					}
				}

				// 偶尔让出CPU，使清理goroutine有机会运行
				if i%10 == 0 {
					time.Sleep(time.Millisecond)
				}
			}
		}(g)
	}

	// 等待所有goroutine完成
	wg.Wait()

	// 再等待一段时间，让清理goroutine有足够时间运行
	time.Sleep(500 * time.Millisecond)
}

// TestStressWithCleanup 测试在高压力下的清理功能
func TestStressWithCleanup(t *testing.T) {
	// 使用较短的过期时间
	locker := NewIDLockerWithExpiration(100 * time.Millisecond)
	defer locker.Close()

	// 跟踪创建的锁ID
	lockIDs := make([]string, 0, 1000)
	for i := 0; i < 1000; i++ {
		id := "stress-cleanup-" + string(rune(i%26+'a')) + string(rune(i/26%26+'a'))
		lockIDs = append(lockIDs, id)
	}

	// 在多个goroutine中执行操作
	var wg sync.WaitGroup
	for g := 0; g < 20; g++ {
		wg.Add(1)
		go func(gID int) {
			defer wg.Done()

			// 每个goroutine运行10轮
			for round := 0; round < 10; round++ {
				// 随机锁定一批ID，然后解锁它们
				startIdx := (gID*50 + round*10) % len(lockIDs)

				// 锁定一批ID
				lockedIDs := make([]string, 0, 10)
				for i := 0; i < 10; i++ {
					id := lockIDs[(startIdx+i)%len(lockIDs)]
					locker.Lock(id)
					lockedIDs = append(lockedIDs, id)
				}

				// 等待一段时间
				time.Sleep(time.Duration(gID%5+1) * time.Millisecond)

				// 解锁这些ID
				for _, id := range lockedIDs {
					locker.Unlock(id)
				}

				// 等待一段时间让清理有机会运行
				if round%3 == 0 {
					time.Sleep(50 * time.Millisecond)
				}
			}
		}(g)
	}

	// 同时，另一个goroutine不断使用TryLock
	wg.Add(1)
	go func() {
		defer wg.Done()

		for i := 0; i < 5000; i++ {
			id := lockIDs[i%len(lockIDs)]
			if locker.TryLock(id) {
				time.Sleep(time.Microsecond)
				locker.Unlock(id)
			}

			if i%100 == 0 {
				time.Sleep(time.Millisecond)
			}
		}
	}()

	// 再创建一个goroutine使用LockWithTimeout
	wg.Add(1)
	go func() {
		defer wg.Done()

		for i := 0; i < 1000; i++ {
			id := lockIDs[(i*7)%len(lockIDs)]
			if locker.LockWithTimeout(id, 2*time.Millisecond) {
				time.Sleep(time.Microsecond)
				locker.Unlock(id)
			}

			if i%50 == 0 {
				time.Sleep(5 * time.Millisecond)
			}
		}
	}()

	// 等待所有goroutine完成
	wg.Wait()

	// 让清理goroutine有足够时间执行
	time.Sleep(200 * time.Millisecond)

	// 检查内部锁的数量
	// 注意：这是一个简化的测试，实际上无法准确预测清理后应该有多少锁
	// 但数量应该远小于我们创建的锁数量
	count := 0
	for range locker.locks.IterBuffered() {
		count++
	}

	t.Logf("清理后剩余锁数量: %d (创建总数: %d)", count, len(lockIDs))
	if count > len(lockIDs)/2 {
		t.Errorf("清理似乎不起作用，预期剩余锁数量应远小于总数的一半")
	}
}
